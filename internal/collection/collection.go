package collection

import "time"

type Collections struct {
	Pink  time.Time `json:"pink"`
	Green time.Time `json:"green"`
	Black time.Time `json:"black"`
}
