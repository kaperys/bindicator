package web

import (
	"context"
	"fmt"
	"time"

	"github.com/chromedp/chromedp"
	"gitlab.com/kaperys/bindicator/internal/collection"
)

var execTimeout = time.Second * 30

func GetNext(ctx context.Context, debug bool, url, postcode, number string) (string, string, error) {
	collections, err := GetCollections(ctx, debug, url, postcode, number)
	if err != nil {
		return "", "", fmt.Errorf("could not get collections: %w", err)
	}

	var (
		nextCollection      string
		nextCollectionDate  string
		nextCollectionDelta float64
	)

	now := time.Now()
	y1, m1, d1 := now.Date()

	for bin, collection := range map[string]time.Time{
		"pink":  collections.Pink,
		"green": collections.Green,
		"black": collections.Black,
	} {
		// If the bin collection is after the time of asking, find the collection which has the
		// smallest delta between now and the collection.
		if collection.After(now) {
			until := collection.Sub(now)

			if until.Seconds() < nextCollectionDelta || nextCollectionDelta == 0 {
				nextCollection = bin
				nextCollectionDate = collection.Format("Monday 2 January 2006")
				nextCollectionDelta = until.Seconds()

				y2, m2, d2 := collection.Date()
				if y1 == y2 && m1 == m2 && d1 == d2 {
					nextCollectionDate = "Today"
				}
			}
		}
	}

	return nextCollection, nextCollectionDate, nil
}

func GetCollections(ctx context.Context, debug bool, url, postcode, number string) (*collection.Collections, error) {
	ctx, cancel := context.WithTimeout(ctx, execTimeout)
	defer cancel()

	if debug {
		ctx, cancel = chromedp.NewExecAllocator(ctx, append(chromedp.DefaultExecAllocatorOptions[:], chromedp.Flag("headless", false))...)
		defer cancel()
	}

	ctx, cancel = chromedp.NewContext(ctx)
	defer cancel()

	// Table Structure
	// |------------------|-----------------------|
	// | Collection day   | Wednesday             |
	// | Pink lid bin day | Wednesday 24 February |
	// | Green bin day    | Wednesday 17 February |
	// | Black bin day    | Wednesday 3 March     |
	// |------------------|-----------------------|

	homeSearchSelector := `//input[@name="q"]`
	postcodeSearchSelector := `//input[@name="keywords"]`
	binCollectionsSelector := `//a[contains(., 'Bin collections')]`
	collectionsCalendarSelector := `//a[contains(., 'Check the bin collection calendar')]`

	var pink, green, black string
	err := chromedp.Run(
		ctx,

		// Load the homepage and search for collections
		chromedp.Navigate(url),
		chromedp.WaitVisible(homeSearchSelector),
		chromedp.SendKeys(homeSearchSelector, "bin collection"),
		chromedp.Submit(homeSearchSelector),

		// Find the bin collections information page
		chromedp.WaitNotPresent(binCollectionsSelector),
		chromedp.Click(binCollectionsSelector, chromedp.BySearch),

		// Click the bin collections calendar link
		chromedp.WaitVisible(collectionsCalendarSelector),
		chromedp.Click(collectionsCalendarSelector, chromedp.BySearch),

		// Search the directory for the given postcode
		chromedp.WaitVisible(postcodeSearchSelector),
		chromedp.SendKeys(postcodeSearchSelector, postcode),
		chromedp.Submit(postcodeSearchSelector),

		// Select the given house number
		chromedp.WaitNotPresent(`//h2[contains(., 'Search the directory')]`),
		chromedp.Click(fmt.Sprintf(`//a[contains(text(),'(%s)')]`, number), chromedp.BySearch),
		chromedp.WaitNotPresent(`//h2[contains(., 'Search results')]`),

		// Load the collection data
		chromedp.Text(".list--definition .definition__content:nth-of-type(3)", &pink, chromedp.ByQuery),
		chromedp.Text(".list--definition .definition__content:nth-of-type(4)", &green, chromedp.ByQuery),
		chromedp.Text(".list--definition .definition__content:nth-of-type(5)", &black, chromedp.ByQuery),
	)
	if err != nil {
		return nil, fmt.Errorf("could not run chromedp: %w", err)
	}

	pinkTs, err := parseCollectionDateTime(pink)
	if err != nil {
		return nil, fmt.Errorf("could not parse pink collection timestamp: %w", err)
	}

	greenTs, err := parseCollectionDateTime(green)
	if err != nil {
		return nil, fmt.Errorf("could not parse green collection timestamp: %w", err)
	}

	blackTs, err := parseCollectionDateTime(black)
	if err != nil {
		return nil, fmt.Errorf("could not parse black collection timestamp: %w", err)
	}

	return &collection.Collections{Pink: pinkTs, Green: greenTs, Black: blackTs}, nil
}

func parseCollectionDateTime(dateTime string) (time.Time, error) {
	// Parse the collection date as provided by rotherham.gov.uk, but, set the collection time
	// to midday, to account for asking the service which bin collection is due on the day of
	// the collection.
	ts, err := time.Parse("Monday 2 January 2006 15:04:05", fmt.Sprintf("%s %d 12:00:00", dateTime, time.Now().Year()))
	if err != nil {
		return time.Time{}, fmt.Errorf("could not parse collection timestamp: %w", err)
	}

	return ts, nil
}
