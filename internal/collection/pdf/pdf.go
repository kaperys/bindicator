package pdf

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"code.sajari.com/docconv"
	"gitlab.com/kaperys/bindicator/internal/collection"
)

type CollectionMonth struct {
	name        string
	collections []string
	dates       []string
}

var collections = []CollectionMonth{
	{name: "December 2021", collections: []string{"pink", "black", "pink"}},
	{name: "January 2022", collections: []string{"black", "pink", "green", "pink"}},
	{name: "February 2022", collections: []string{"black", "pink", "green", "pink"}},
	{name: "March 2022", collections: []string{"black", "pink", "green", "pink", "black"}},
	{name: "April 2022", collections: []string{"pink", "green", "pink", "black"}},
	{name: "May 2022", collections: []string{"pink", "green", "pink", "black"}},
	{name: "June 2022", collections: []string{"pink", "green", "pink", "black", "pink"}},
	{name: "July 2022", collections: []string{"green", "pink", "black", "pink"}},
	{name: "August 2022", collections: []string{"green", "pink", "black", "pink", "green"}},
	{name: "September 2022", collections: []string{"pink", "black", "pink", "green"}},
	{name: "October 2022", collections: []string{"pink", "black", "pink", "green"}},
	{name: "November 2022", collections: []string{"black", "pink", "green", "pink"}},
}

func Load(calendar, collectionDay string) error {
	data, err := docconv.ConvertPath(calendar)
	if err != nil {
		return fmt.Errorf("could not load calendar data: %w", err)
	}

	var (
		record bool
		dates  []string
		period int
	)

	for _, v := range strings.Split(data.Body, "\n") {
		switch {
		case v == collectionDay:
			record = true
			if len(dates) > 0 {
				collections[period].dates = dates
				dates = []string{}
				period++
			}
		case v != collectionDay && isDay(v):
			record = false
		case record && isDate(v):
			dates = append(dates, v)
		}
	}

	collections[period].dates = dates
	return nil
}

func isDate(in string) bool {
	if strings.TrimSpace(in) == "" {
		return false
	}

	_, err := time.Parse("2", in)
	return err == nil
}

func isDay(in string) bool {
	if strings.TrimSpace(in) == "" {
		return false
	}

	_, err := time.Parse("Mon", in)
	return err == nil
}

func GetNext() (string, string, error) {
	month := collections[time.Now().Month()]

	currentMonth, err := time.Parse("January 2006 2", month.name+" "+month.dates[len(month.dates)-1])
	if err != nil {
		return "", "", fmt.Errorf("could not parse current month: %w", err)
	}

	currentMonth = time.Date(currentMonth.Year(), currentMonth.Month(), currentMonth.Day(), 12, 0, 0, 0, time.Local)
	if time.Now().After(currentMonth) {
		if int(time.Now().Month()+1) > len(collections) {
			return "", "", fmt.Errorf("collection month out of range")
		}

		month = collections[time.Now().Month()+1]
	}

	for i, date := range month.dates {
		if d, _ := strconv.Atoi(date); d >= time.Now().Day() {
			ts, err := time.Parse("January 2006 2", month.name+" "+date)
			if err != nil {
				return "", "", fmt.Errorf("could not parse collection month: %w", err)
			}

			return month.collections[i], ts.Format("Monday 2 January 2006"), nil
		}
	}

	return "", "", nil
}

func GetCollections() (*collection.Collections, error) {
	m := time.Now().Month()
	d := time.Now().Day()

	var pink, green, black *time.Time
	for {
		month := collections[m]
		if pink != nil && green != nil && black != nil {
			break
		}

		for i, date := range month.dates {
			if di, _ := strconv.Atoi(date); di < d && d != 0 {
				continue
			}
			d = 0

			switch month.collections[i] {
			case "pink":
				if pink == nil {
					ts, err := time.Parse("January 2006 2", month.name+" "+date)
					if err != nil {
						return nil, fmt.Errorf("could not parse pink collection: %w", err)
					}

					ts = time.Date(ts.Year(), ts.Month(), ts.Day(), 12, 0, 0, 0, time.Local)
					pink = &ts
				}
			case "green":
				if green == nil {
					ts, err := time.Parse("January 2006 2", month.name+" "+date)
					if err != nil {
						return nil, fmt.Errorf("could not parse green collection: %w", err)
					}

					ts = time.Date(ts.Year(), ts.Month(), ts.Day(), 12, 0, 0, 0, time.Local)
					green = &ts
				}
			case "black":
				if black == nil {
					ts, err := time.Parse("January 2006 2", month.name+" "+date)
					if err != nil {
						return nil, fmt.Errorf("could not parse black collection: %w", err)
					}

					ts = time.Date(ts.Year(), ts.Month(), ts.Day(), 12, 0, 0, 0, time.Local)
					black = &ts
				}
			}
		}

		m++
	}

	return &collection.Collections{Pink: *pink, Green: *green, Black: *black}, nil
}
