package api

import (
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"net"
	"net/http"
	"os"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/kaperys/bindicator/internal/collection"
	"gitlab.com/kaperys/bindicator/internal/collection/pdf"
	"gitlab.com/kaperys/bindicator/internal/collection/web"
	"gitlab.com/kaperys/bindicator/internal/embed/templates"
)

const (
	url = "https://www.rotherham.gov.uk/"

	defaultIp   = "0.0.0.0"
	defaultPort = "8080"
)

var (
	lastCollection, lastDate string                  // local cache
	lastCollectionDates      *collection.Collections // local cache
)

func NewHTTP(ctx context.Context, debug bool) error {
	ip := os.Getenv("IP")
	if ip == "" {
		ip = defaultIp
	}

	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	logrus.Debugf("listening on %s:%s", ip, port)
	http.HandleFunc("/", serveWeb(ctx, debug))
	http.HandleFunc("/json", serveJSON(ctx, debug))
	http.HandleFunc("/v2", serveWebV2(ctx, debug))
	http.HandleFunc("/v2/json", serveJSONV2(ctx, debug))

	return http.ListenAndServe(net.JoinHostPort(ip, port), nil)
}

func serveWeb(ctx context.Context, debug bool) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if !auth(r) {
			http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
			return
		}

		postcode, number, err := parseRequest(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		if lastCollection != "" {
			err := render(w, lastCollection, lastDate)
			if err != nil {
				logrus.WithError(err).Error("could not render results")
				http.Error(w, "could not render results", http.StatusInternalServerError)
				return
			}

			go func() {
				logrus.Debug("attempting to retrieve next bin collection")
				collection, date, err := web.GetNext(ctx, debug, url, postcode, number)
				if err != nil {
					logrus.WithError(err).Error("could not get next collection")
					return
				}

				lastCollection, lastDate = collection, date
				logrus.Debugf("updated cached collection: %s on %s", lastCollection, lastDate)
			}()
		} else {
			logrus.Debug("attempting to retrieve next bin collection")
			collection, date, err := web.GetNext(r.Context(), debug, url, postcode, number)
			if err != nil {
				logrus.WithError(err).Error("could not get next collection")
				http.Error(w, "could not get next collection", http.StatusInternalServerError)
				return
			}

			lastCollection, lastDate = collection, date
			err = render(w, collection, date)
			if err != nil {
				logrus.WithError(err).Error("could not render results")
				http.Error(w, "could not render results", http.StatusInternalServerError)
				return
			}

			logrus.Debugf("updated cached collection: %s on %s", lastCollection, lastDate)
		}
	}
}

func serveJSON(ctx context.Context, debug bool) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if !auth(r) {
			http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
			return
		}

		postcode, number, err := parseRequest(r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		if lastCollectionDates != nil {
			err = json.NewEncoder(w).Encode(lastCollectionDates)
			if err != nil {
				logrus.WithError(err).Error("could not render results")
				http.Error(w, "could not render results", http.StatusInternalServerError)
				return
			}

			go func() {
				logrus.Debug("attempting to retrieve next bin collection")
				collections, err := web.GetCollections(ctx, debug, url, postcode, number)
				if err != nil {
					logrus.WithError(err).Error("could not get next collection")
					return
				}

				lastCollectionDates = collections
				logrus.Debug("updated cached collections")
			}()
		} else {
			logrus.Debug("attempting to retrieve next bin collection")
			collections, err := web.GetCollections(r.Context(), debug, url, postcode, number)
			if err != nil {
				logrus.WithError(err).Error("could not get next collection")
				http.Error(w, "could not get next collection", http.StatusInternalServerError)
				return
			}

			lastCollectionDates = collections
			err = json.NewEncoder(w).Encode(collections)
			if err != nil {
				logrus.WithError(err).Error("could not render results")
				http.Error(w, "could not render results", http.StatusInternalServerError)
				return
			}

			logrus.Debug("updated cached collections")
		}
	}
}

func serveWebV2(ctx context.Context, debug bool) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if !auth(r) {
			http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
			return
		}

		collection, date, err := pdf.GetNext()
		if err != nil {
			logrus.WithError(err).Error("could not get next collection")
			http.Error(w, "could not get next collection", http.StatusInternalServerError)
			return
		}

		err = render(w, collection, date)
		if err != nil {
			logrus.WithError(err).Error("could not render results")
			http.Error(w, "could not render results", http.StatusInternalServerError)
			return
		}
	}
}

func serveJSONV2(ctx context.Context, debug bool) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if !auth(r) {
			http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
			return
		}

		collections, err := pdf.GetCollections()
		if err != nil {
			logrus.WithError(err).Error("could not get next collection")
			http.Error(w, "could not get next collection", http.StatusInternalServerError)
			return
		}

		err = json.NewEncoder(w).Encode(collections)
		if err != nil {
			logrus.WithError(err).Error("could not render results")
			http.Error(w, "could not render results", http.StatusInternalServerError)
			return
		}
	}
}

func auth(r *http.Request) bool {
	return r.URL.Query().Get("token") == os.Getenv("TOKEN")
}

func parseRequest(r *http.Request) (string, string, error) {
	postcode := strings.TrimSpace(r.URL.Query().Get("postcode"))
	if postcode == "" {
		return "", "", fmt.Errorf("postcode is required")
	}

	number := strings.TrimSpace(r.URL.Query().Get("number"))
	if number == "" {
		return "", "", fmt.Errorf("number is required")
	}

	return postcode, number, nil
}

func render(w http.ResponseWriter, collection, date string) error {
	logrus.Debugf("rendering collection: %s on %s", lastCollection, lastDate)

	tmpl, err := template.ParseFS(templates.Static, "index.html")
	if err != nil {
		return fmt.Errorf("could not load template: %w", err)
	}

	type view struct {
		Date       string
		Collection string
	}

	err = tmpl.Execute(w, view{Date: date, Collection: collection})
	if err != nil {
		return fmt.Errorf("could not execute template: %w", err)
	}

	return nil
}
