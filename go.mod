module gitlab.com/kaperys/bindicator

go 1.16

require (
	code.sajari.com/docconv v1.2.0 // indirect
	github.com/chromedp/cdproto v0.0.0-20210625233425-810000e4a4fc // indirect
	github.com/chromedp/chromedp v0.7.3
	github.com/ledongthuc/pdf v0.0.0-20220302134840-0c2507a12d80 // indirect
	github.com/sirupsen/logrus v1.6.0
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
)
