package main

import (
	"context"
	"os"

	"github.com/sirupsen/logrus"
	"gitlab.com/kaperys/bindicator/internal/api"
	"gitlab.com/kaperys/bindicator/internal/collection/pdf"
)

func main() {
	err := run(context.Background())
	if err != nil {
		logrus.WithError(err).Panic("could not run bindicator")
	}
}

func run(ctx context.Context) error {
	logrus.Info("running bin collection app")

	debug := os.Getenv("DEBUG") == "true"
	if debug {
		logrus.SetLevel(logrus.DebugLevel)
		logrus.Debug("debug mode enabled")
	}

	err := pdf.Load(os.Getenv("PDF_COLLECTION_CALENDAR"), os.Getenv("COLLECTION_DAY"))
	if err != nil {
		return err
	}

	return api.NewHTTP(ctx, debug)
}
