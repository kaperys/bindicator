package main

import (
	"strings"
	"time"

	"code.sajari.com/docconv"
)

const collectionDay = "Wed"

type CollectionMonth struct {
	name        string
	collections []string
	dates       []string
}

var collections = []CollectionMonth{
	{name: "December 2021", collections: []string{"pink", "black", "pink"}},
	{name: "January 2022", collections: []string{"black", "pink", "green", "pink"}},
	{name: "February 2022", collections: []string{"black", "pink", "green", "pink"}},
	{name: "March 2022", collections: []string{"black", "pink", "green", "pink", "black"}},
	{name: "April 2022", collections: []string{"pink", "green", "pink", "black"}},
	{name: "May 2022", collections: []string{"pink", "green", "pink", "black"}},
	{name: "June 2022", collections: []string{"pink", "green", "pink", "black", "pink"}},
	{name: "July 2022", collections: []string{"green", "pink", "black", "pink"}},
	{name: "August 2022", collections: []string{"green", "pink", "black", "pink", "green"}},
	{name: "September 2022", collections: []string{"pink", "black", "pink", "green"}},
	{name: "October 2022", collections: []string{"pink", "black", "pink", "green"}},
	{name: "November 2022", collections: []string{"pink", "black", "pink", "green", "pink"}},
}

func main() {
	data, err := docconv.ConvertPath("schedule.pdf")
	if err != nil {
		panic(err)
	}

	var (
		record bool
		dates  []string
		period int
	)

	for _, v := range strings.Split(data.Body, "\n") {
		switch {
		case v == collectionDay:
			record = true
			if len(dates) > 0 {
				collections[period].dates = dates
				dates = []string{}
				period++
			}
		case v != collectionDay && isDay(v):
			record = false
		case record && isDate(v):
			dates = append(dates, v)
		}
	}

	collections[period].dates = dates
	for _, f := range collections {
		println(f.name)
		for i := 0; i < len(f.collections); i++ {
			println(f.dates[i], f.collections[i])
		}
	}
}

func isDate(in string) bool {
	if strings.TrimSpace(in) == "" {
		return false
	}

	_, err := time.Parse("2", in)
	return err == nil
}

func isDay(in string) bool {
	if strings.TrimSpace(in) == "" {
		return false
	}

	_, err := time.Parse("Mon", in)
	return err == nil
}
