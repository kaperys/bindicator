FROM golang:1.16.5-stretch AS build
ARG SERVICE
WORKDIR /go/src/github.com/kaperys/${SERVICE}

RUN apt-get update && apt-get install make git
COPY go.* ./
RUN go mod download
COPY . .
RUN make build

FROM chromedp/headless-shell:latest
ARG SERVICE
ENV APP=${SERVICE}

RUN apt-get update && apt-get install -y dumb-init ca-certificates poppler-utils

COPY --from=build /go/src/github.com/kaperys/${SERVICE}/${SERVICE} /app/${SERVICE}

EXPOSE 8080
ENTRYPOINT ["dumb-init", "--"]
CMD /app/${APP}
