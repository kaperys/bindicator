mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
base_dir := $(notdir $(patsubst %/,%,$(dir $(mkfile_path))))

SERVICE ?= $(base_dir)
GIT_HASH := $(shell git rev-parse HEAD)

.PHONY: fmt
fmt:
	@gofumports -w . && gofumpt -w -s .

.PHONY: build
build:
	@go build ./cmd/$(SERVICE)

.PHONY: build-docker-image
build-docker-image:
	docker build -t $(SERVICE):local . --build-arg SERVICE=$(SERVICE)

.PHONY: push-docker-image
push-docker-image:
	docker tag $(SERVICE):local registry.gitlab.com/kaperys/$(SERVICE):$(GIT_HASH)
	docker push registry.gitlab.com/kaperys/$(SERVICE):$(GIT_HASH)
