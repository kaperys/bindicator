# bindicator

https://bindicator.kaperys.io/

## /

Next collection as HTML, designed to be embedded into applications like Home Assistant via an iframe.

## /json

All next bin collections as json, designed to be embedded into applications like Home Assistant using rest sensors.
